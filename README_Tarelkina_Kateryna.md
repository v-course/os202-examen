# OS202. Examen 12.03.2024. Kateryna TARELKINA

## lscpu

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.1
                                            0GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
Caches (sum of all):
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## Exécuter

- colorize

```
python3 colorize.py
```

- colorize1

```
mpirun -n [number_of_processes] python3 ./colorize1.py
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./colorize1.py
```

- colorize2

```
mpirun -n [number_of_processes] python3 ./colorize2.py
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./colorize2.py
```

Pour mésurer le temps, ajouter `time` avant la commande :

```
time python3 colorize.py
```

## Résultats

- colorize1

Pour cette version, on a coupé l'image en nbp parties et chaque processus a coloré sa partie. Ensuite le processus racine a réuni toutes les parties ensemble pour créer une image finale.

Pour mésurer le temps de calcul, on a ajouté `time` avant la commande et on a analysé le paramètre `real` dans le output :

```
time mpirun --hostfile hostfile -n [number_of_processes] python3 ./colorize1.py
```

et

```
time python3 ./colorize.py
```

Les résultats obtenus :

| Le nombre de threads | Le temps d'exécution | Le speedup |
| -------------------- | -------------------- | ---------- |
| 1                    | 2,516666667          | 1,00       |
| 2                    | 1,616666667          | 1,56       |
| 3                    | 2,5                  | 1,01       |
| 4                    | 2,733333333          | 0,92       |

- colorize2

Pour cette version, la strategie maître-esclave a été utilisée, avec le nombre de tâches égale au nombre de processus (c'est-à-dire chaque processus-esclave n'obtient qu'une tâche). Processus-maître est celui responsable pour le calcul et la construction de l'image finale. Quand on doit faire un produit matrice-vecteur, le processus principal divise la matrice en fragmenets (par lignes) et la repartie entre les processus-esclave pour qu'ils calculent la partie du vecteur de résultat.
